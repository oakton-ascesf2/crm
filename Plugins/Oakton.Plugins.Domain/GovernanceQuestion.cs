﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;

namespace Oakton.Plugins.Domain
{
    public class GovernanceQuestion
    {
        private List<GovernanceQuestion> _childQuestions = new List<GovernanceQuestion>();
        public int SiblingCount { get; set; }
        public Guid Id { get; set; } //id
        public string LogicalName { get; private set; }
        public string Name { get; private set; }
        public bool AllCategories { get; private set; }    //okn_asprall
        public bool CategoryA { get; private set; }//okn_categorya
        public bool CategoryB { get; private set; }//okn_categoryb
        public bool Top23 { get; private set; }//okn_top23        
        public bool ForASPR { get; private set; } //okn_foraspr
        public GovernanceAnswerTypeEnum AnswerType { get; private set; } //okn_answertype
        public Decimal? QuestionValue { get; private set; } //okn_questionvalue
        public bool IsParent { get; private set; } //okn_isparent
        public Decimal? LowestAnswerValue { get; private set; } //okn_lowestanswervalue
        public Guid ParentId { get; set; } //okn_parentquestion
        public Decimal? LowAnswerValue { get; private set; } //okn_lowanswervalue
        public Decimal? MiddleAnswerValue { get; private set; }  //okn_middleanswervalue
        public Decimal? HighAnswerValue { get; private set; } //okn_highanswervalue
        public Decimal? HighestAnswerValue { get; private set; } //okn_highestanswervalue
        public Decimal? LowestPercentageRange { get; private set; } //okn_lowestpercentagerange
        public Decimal? HighestPercentageRange { get; private set; } //okn_highestpercentagerange

        public GovernanceQuestion(Entity entity, IOrganizationService service)
        {
            Id = entity.Id;

            LogicalName = entity.LogicalName;
            Name = entity.GetAttributeValue<string>("okn_name");
            AllCategories = entity.GetAttributeValue<bool>("okn_asprall");
            CategoryA = entity.GetAttributeValue<bool>("okn_categorya");
            CategoryB = entity.GetAttributeValue<bool>("okn_categoryb");
            Top23 = entity.GetAttributeValue<bool>("okn_top23");
            ForASPR = entity.GetAttributeValue<bool>("okn_foraspr");

            OptionSetValue value = entity.GetAttributeValue<OptionSetValue>("okn_answertype");

            if (value == null)
                AnswerType = GovernanceAnswerTypeEnum.None;
            else
                AnswerType = (GovernanceAnswerTypeEnum)(entity.GetAttributeValue<OptionSetValue>("okn_answertype").Value);

            QuestionValue = entity.GetAttributeValue<Decimal?>("okn_questionvalue");
            IsParent = entity.GetAttributeValue<bool>("okn_isparent");

            LowestAnswerValue = entity.GetAttributeValue<Decimal?>("okn_lowestanswervalue");
            LowAnswerValue = entity.GetAttributeValue<Decimal?>("okn_lowanswervalue");
            MiddleAnswerValue = entity.GetAttributeValue<Decimal?>("okn_middleanswervalue");
            HighAnswerValue = entity.GetAttributeValue<Decimal?>("okn_highanswervalue");
            HighestAnswerValue = entity.GetAttributeValue<Decimal?>("okn_highestanswervalue");
            LowestPercentageRange = entity.GetAttributeValue<Decimal?>("okn_lowestpercentagerange");
            HighestPercentageRange = entity.GetAttributeValue<Decimal?>("okn_highestpercentagerange");

            if (!IsParent)
            {
                EntityReference parent = entity.GetAttributeValue<EntityReference>("okn_parentquestion");
                if (parent != null)
                    ParentId = parent.Id;

              //  AnswerType = new Persistence.GovernanceQuestions().GetParentAnswerType(service, ParentId);
            }

        }

        public void SetParentAnswerType(GovernanceAnswerTypeEnum answerType)
        {
            AnswerType = answerType;
        }

        public void SetValues(GovernanceQuestion parentGovernanceQuestion)
        {
            if (parentGovernanceQuestion.LowestAnswerValue != null && SiblingCount > 0)
                LowestAnswerValue = parentGovernanceQuestion.LowestAnswerValue / SiblingCount;
            if (parentGovernanceQuestion.LowAnswerValue != null && SiblingCount > 0)
                LowAnswerValue = parentGovernanceQuestion.LowAnswerValue / SiblingCount;
            if (parentGovernanceQuestion.MiddleAnswerValue != null && SiblingCount > 0)
                MiddleAnswerValue = parentGovernanceQuestion.MiddleAnswerValue / SiblingCount;
            if (parentGovernanceQuestion.HighAnswerValue != null && SiblingCount > 0)
                HighAnswerValue = parentGovernanceQuestion.HighAnswerValue / SiblingCount;
            if (parentGovernanceQuestion.HighestAnswerValue != null && SiblingCount > 0)
                HighestAnswerValue = parentGovernanceQuestion.HighestAnswerValue / SiblingCount;

            //LowestPercentageRange = parentGovernanceQuestion.LowestPercentageRange;
            //HighestPercentageRange = parentGovernanceQuestion.HighestPercentageRange;

            QuestionValue = parentGovernanceQuestion.QuestionValue / SiblingCount;
        }
    }
}
