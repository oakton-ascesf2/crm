﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.Domain
{
    public class FinancialAssessment
    {
        public Guid Id { get; private set; }
        public Guid OrganisationId { get; private set; }

        public FinancialAssessment(Entity entity)
        {
            Id = entity.Id;
            OrganisationId = entity.GetAttributeValue<EntityReference>("okn_organisation").Id;
        }
    }
}
