﻿using Microsoft.Xrm.Sdk;
using System;

namespace Oakton.Plugins.Domain
{
    public class FinancialAssessmentQuestionConfig
    {
        public Guid CategoryId { get; private set; }
        public Guid Id { get; private set; }
        public Guid SubCategoryId { get; private set; }
        public Guid TopLevelCategoryId { get; private set; }
        public string CriteriaHigher { get; private set; }
        public string CriteriaLower { get; private set; }
        public string CriteriaModerate { get; private set; }
        public string Description { get; private set; }
        public string Name { get; private set; }
        public string Question { get; private set; }
        public Decimal? QuestionWeight { get; private set; }
        public Decimal? RangeHighRiskMax { get; set; }
        public Decimal? RangeHighRiskMin { get; set; }
        public Decimal? RangeLowerRiskMax { get; set; }
        public Decimal? RangeLowerRiskMin { get; set; }
        public Decimal? RangeModerateRiskMax { get; set; }
        public Decimal? RangeModerateRiskMin { get; set; }
        public bool? ForCategoryA { get; private set; }
        public bool? ForCategoryB { get; private set; }
        public bool? ForCategoryC { get; private set; }




        /*
         okn_forcategorya
         okn_forcategoryb
         okn_forcategoryc
        okn_category
        okn_financeassessmentquestionconfigid
        okn_subcategory
        okn_toplevelcategory          
        okn_criteriahigher
        okn_criterialower
        okn_criteriamoderate
        okn_description        
        okn_name
        okn_question
        okn_questionweight
        okn_rangehighriskmax
        okn_rangehighriskmin
        okn_rangelowerriskmax
        okn_rangelowerriskmin
        okn_rangemoderateriskmax
        okn_rangemoderateriskmin
        */
        
        public FinancialAssessmentQuestionConfig(Entity entity)
        {
            Id = entity.Id;
            CategoryId = entity.GetAttributeValue<EntityReference>("okn_category") == null ? default(Guid) : entity.GetAttributeValue<EntityReference>("okn_category").Id;
            SubCategoryId = entity.GetAttributeValue<EntityReference>("okn_subcategory") == null ? default(Guid) : entity.GetAttributeValue<EntityReference>("okn_subcategory").Id;
            TopLevelCategoryId  = entity.GetAttributeValue<EntityReference>("okn_toplevelcategory") == null ? default(Guid) : entity.GetAttributeValue<EntityReference>("okn_toplevelcategory").Id;

            CriteriaHigher = entity.GetAttributeValue<string>("okn_criteriahigher");
            CriteriaLower = entity.GetAttributeValue<string>("okn_criterialower");
            CriteriaModerate  = entity.GetAttributeValue<string>("okn_criteriamoderate");

            Description = entity.GetAttributeValue<string>("okn_description");
            Name  = entity.GetAttributeValue<string>("okn_name");
            Question= entity.GetAttributeValue<string>("okn_question");
            QuestionWeight = entity.GetAttributeValue<Decimal?>("okn_questionweight");
            RangeHighRiskMax  = entity.GetAttributeValue<Decimal?>("okn_rangehighriskmax");
            RangeHighRiskMin = entity.GetAttributeValue<Decimal?>("okn_rangehighriskmin");
            RangeLowerRiskMax  = entity.GetAttributeValue<Decimal?>("okn_rangelowerriskmax");
            RangeLowerRiskMin = entity.GetAttributeValue<Decimal?>("okn_rangelowerriskmin");
            RangeModerateRiskMax = entity.GetAttributeValue<Decimal?>("okn_rangemoderateriskmax");
            RangeModerateRiskMin = entity.GetAttributeValue<Decimal?>("okn_rangemoderateriskmin");


            ForCategoryA = entity.GetAttributeValue<bool?>("okn_forcategorya");
            ForCategoryB = entity.GetAttributeValue<bool?>("okn_forcategoryb");
            ForCategoryC = entity.GetAttributeValue<bool?>("okn_forcategoryb");



        }
    }
}
