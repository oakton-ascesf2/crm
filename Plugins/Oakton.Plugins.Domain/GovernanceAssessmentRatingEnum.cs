﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.Domain
{
    public enum GovernanceAssessmentRatingEnum
    {
        NotInPlace = 967300000,
        Adopting = 967300002,
        Implemented = 967300001
    }
}
