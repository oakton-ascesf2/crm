﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.Domain
{


    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://abr.business.gov.au/ABRXMLSearch/", IsNullable = false)]
    public partial class ABRPayloadSearchResults
    {

        private ABRPayloadSearchResultsRequest requestField;

        private ABRPayloadSearchResultsResponse responseField;

        /// <remarks/>
        public ABRPayloadSearchResultsRequest request
        {
            get
            {
                return this.requestField;
            }
            set
            {
                this.requestField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponse response
        {
            get
            {
                return this.responseField;
            }
            set
            {
                this.responseField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsRequest
    {

        private ABRPayloadSearchResultsRequestIdentifierSearchRequest identifierSearchRequestField;

        /// <remarks/>
        public ABRPayloadSearchResultsRequestIdentifierSearchRequest identifierSearchRequest
        {
            get
            {
                return this.identifierSearchRequestField;
            }
            set
            {
                this.identifierSearchRequestField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsRequestIdentifierSearchRequest
    {

        private string authenticationGUIDField;

        private string identifierTypeField;

        private ulong identifierValueField;

        private string historyField;

        /// <remarks/>
        public string authenticationGUID
        {
            get
            {
                return this.authenticationGUIDField;
            }
            set
            {
                this.authenticationGUIDField = value;
            }
        }

        /// <remarks/>
        public string identifierType
        {
            get
            {
                return this.identifierTypeField;
            }
            set
            {
                this.identifierTypeField = value;
            }
        }

        /// <remarks/>
        public ulong identifierValue
        {
            get
            {
                return this.identifierValueField;
            }
            set
            {
                this.identifierValueField = value;
            }
        }

        /// <remarks/>
        public string history
        {
            get
            {
                return this.historyField;
            }
            set
            {
                this.historyField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponse
    {

        private string usageStatementField;

        private System.DateTime dateRegisterLastUpdatedField;

        private System.DateTime dateTimeRetrievedField;

        private ABRPayloadSearchResultsResponseBusinessEntity201408 businessEntity201408Field;

        private ABRPayloadSearchResultsResponseException exceptionField;

        /// <remarks/>
        public string usageStatement
        {
            get
            {
                return this.usageStatementField;
            }
            set
            {
                this.usageStatementField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime dateRegisterLastUpdated
        {
            get
            {
                return this.dateRegisterLastUpdatedField;
            }
            set
            {
                this.dateRegisterLastUpdatedField = value;
            }
        }

        /// <remarks/>
        public System.DateTime dateTimeRetrieved
        {
            get
            {
                return this.dateTimeRetrievedField;
            }
            set
            {
                this.dateTimeRetrievedField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity201408 businessEntity201408
        {
            get
            {
                return this.businessEntity201408Field;
            }
            set
            {
                this.businessEntity201408Field = value;
            }
        }


        /// <remarks/>
        public ABRPayloadSearchResultsResponseException exception
        {
            get
            {
                return this.exceptionField;
            }
            set
            {
                this.exceptionField = value;
            }
        }
    }


    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseBusinessEntity201408
    {

        private System.DateTime recordLastUpdatedDateField;

        private ABRPayloadSearchResultsResponseBusinessEntity201408ABN aBNField;

        private ABRPayloadSearchResultsResponseBusinessEntity201408EntityStatus entityStatusField;

        private object aSICNumberField;

        private ABRPayloadSearchResultsResponseBusinessEntity201408EntityType entityTypeField;

        private ABRPayloadSearchResultsResponseBusinessEntity201408MainName mainNameField;

        private ABRPayloadSearchResultsResponseBusinessEntity201408MainTradingName mainTradingNameField;

        private ABRPayloadSearchResultsResponseBusinessEntity201408MainBusinessPhysicalAddress mainBusinessPhysicalAddressField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime recordLastUpdatedDate
        {
            get
            {
                return this.recordLastUpdatedDateField;
            }
            set
            {
                this.recordLastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity201408ABN ABN
        {
            get
            {
                return this.aBNField;
            }
            set
            {
                this.aBNField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity201408EntityStatus entityStatus
        {
            get
            {
                return this.entityStatusField;
            }
            set
            {
                this.entityStatusField = value;
            }
        }

        /// <remarks/>
        public object ASICNumber
        {
            get
            {
                return this.aSICNumberField;
            }
            set
            {
                this.aSICNumberField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity201408EntityType entityType
        {
            get
            {
                return this.entityTypeField;
            }
            set
            {
                this.entityTypeField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity201408MainName mainName
        {
            get
            {
                return this.mainNameField;
            }
            set
            {
                this.mainNameField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity201408MainTradingName mainTradingName
        {
            get
            {
                return this.mainTradingNameField;
            }
            set
            {
                this.mainTradingNameField = value;
            }
        }




        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity201408MainBusinessPhysicalAddress mainBusinessPhysicalAddress
        {
            get
            {
                return this.mainBusinessPhysicalAddressField;
            }
            set
            {
                this.mainBusinessPhysicalAddressField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseException
    {

        private string exceptionDescriptionField;

        private string exceptionCodeField;

        /// <remarks/>
        public string exceptionDescription
        {
            get
            {
                return this.exceptionDescriptionField;
            }
            set
            {
                this.exceptionDescriptionField = value;
            }
        }

        /// <remarks/>
        public string exceptionCode
        {
            get
            {
                return this.exceptionCodeField;
            }
            set
            {
                this.exceptionCodeField = value;
            }
        }
    }


}


/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
public partial class ABRPayloadSearchResultsResponseBusinessEntity201408ABN
{

    private ulong identifierValueField;

    private string isCurrentIndicatorField;

    private System.DateTime replacedFromField;

    /// <remarks/>
    public ulong identifierValue
    {
        get
        {
            return this.identifierValueField;
        }
        set
        {
            this.identifierValueField = value;
        }
    }

    /// <remarks/>
    public string isCurrentIndicator
    {
        get
        {
            return this.isCurrentIndicatorField;
        }
        set
        {
            this.isCurrentIndicatorField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
    public System.DateTime replacedFrom
    {
        get
        {
            return this.replacedFromField;
        }
        set
        {
            this.replacedFromField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
public partial class ABRPayloadSearchResultsResponseBusinessEntity201408EntityStatus
{

    private string entityStatusCodeField;

    private System.DateTime effectiveFromField;

    private System.DateTime effectiveToField;

    /// <remarks/>
    public string entityStatusCode
    {
        get
        {
            return this.entityStatusCodeField;
        }
        set
        {
            this.entityStatusCodeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
    public System.DateTime effectiveFrom
    {
        get
        {
            return this.effectiveFromField;
        }
        set
        {
            this.effectiveFromField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
    public System.DateTime effectiveTo
    {
        get
        {
            return this.effectiveToField;
        }
        set
        {
            this.effectiveToField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
public partial class ABRPayloadSearchResultsResponseBusinessEntity201408EntityType
{

    private string entityTypeCodeField;

    private string entityDescriptionField;

    /// <remarks/>
    public string entityTypeCode
    {
        get
        {
            return this.entityTypeCodeField;
        }
        set
        {
            this.entityTypeCodeField = value;
        }
    }

    /// <remarks/>
    public string entityDescription
    {
        get
        {
            return this.entityDescriptionField;
        }
        set
        {
            this.entityDescriptionField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
public partial class ABRPayloadSearchResultsResponseBusinessEntity201408MainName
{

    private string organisationNameField;

    private System.DateTime effectiveFromField;

    /// <remarks/>
    public string organisationName
    {
        get
        {
            return this.organisationNameField;
        }
        set
        {
            this.organisationNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
    public System.DateTime effectiveFrom
    {
        get
        {
            return this.effectiveFromField;
        }
        set
        {
            this.effectiveFromField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
public partial class ABRPayloadSearchResultsResponseBusinessEntity201408MainTradingName
{

    private string organisationNameField;

    private System.DateTime effectiveFromField;

    /// <remarks/>
    public string organisationName
    {
        get
        {
            return this.organisationNameField;
        }
        set
        {
            this.organisationNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
    public System.DateTime effectiveFrom
    {
        get
        {
            return this.effectiveFromField;
        }
        set
        {
            this.effectiveFromField = value;
        }
    }
}


/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
public partial class ABRPayloadSearchResultsResponseBusinessEntity201408MainBusinessPhysicalAddress
{

    private string stateCodeField;

    private ushort postcodeField;

    private System.DateTime effectiveFromField;

    private System.DateTime effectiveToField;

    /// <remarks/>
    public string stateCode
    {
        get
        {
            return this.stateCodeField;
        }
        set
        {
            this.stateCodeField = value;
        }
    }

    /// <remarks/>
    public ushort postcode
    {
        get
        {
            return this.postcodeField;
        }
        set
        {
            this.postcodeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
    public System.DateTime effectiveFrom
    {
        get
        {
            return this.effectiveFromField;
        }
        set
        {
            this.effectiveFromField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
    public System.DateTime effectiveTo
    {
        get
        {
            return this.effectiveToField;
        }
        set
        {
            this.effectiveToField = value;
        }
    }
}
