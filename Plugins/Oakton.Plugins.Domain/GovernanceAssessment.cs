﻿using Microsoft.Xrm.Sdk;
using System;

namespace Oakton.Plugins.Domain
{
    public class GovernanceAssessment
    {

        public Guid Id { get; private set; }
        public string LogicalName { get; private set; }
        public Guid OrganisationId { get; private set; } //okn_organisationid
        public DateTime AssessmentDate { get; private set; }

        public GovernanceAssessment(Entity entity)
        {
            Id = entity.Id;
            LogicalName = entity.LogicalName;
            OrganisationId = entity.GetAttributeValue<EntityReference>("okn_organisation").Id;
            AssessmentDate = entity.GetAttributeValue<DateTime>("okn_assessmentdate");
            
        }
    }
}
