﻿namespace Oakton.Plugins.Domain
{
    public enum GovernanceAnswerTypeEnum
    {
        None = 0,
        Text = 967300000,
        Number = 967300001,
        Percentage = 967300002,
        TwoPointScaleImplemented = 967300003,
        TwoPointScaleImplementedNA = 967300007,
        TwoPointScaleAchieved = 967300006,
        TwoPointScaleAchievedNA = 967300008,
        ThreePointScale = 967300004,
        ThreePointScaleNA = 967300009,
        FivePointScaleAgreement = 967300005,
        FivePointScaleFrequency = 967300010
    }
}
