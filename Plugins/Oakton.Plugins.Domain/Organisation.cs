﻿using Microsoft.Xrm.Sdk;
using Oakton.Plugins.Domain;
using System;

namespace Oakton.Plugins.Domain
{
    public class Organisation
    {
        public bool IsTop24 { private set; get; } //okn_top23
        public bool IsCategoryA { private set; get; } //okn_governancecategory

        public Guid Id { get; private set; }
        public FinancialCategoryEnum FinancialCategory { get; private set; }

        public Organisation(Entity entity)
        {
            IsTop24 = entity.GetAttributeValue<bool>("okn_top23");
            IsCategoryA = !entity.GetAttributeValue<bool>("okn_governancecategory");
            Id = entity.Id;

            if (entity.GetAttributeValue<OptionSetValue>("okn_financialcategory") == null)
                throw new InvalidPluginExecutionException("An organisation must have a financial category.");
            
            FinancialCategory = (FinancialCategoryEnum)(entity.GetAttributeValue<OptionSetValue>("okn_financialcategory").Value);
        }
    }
}