﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Oakton.Plugins.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.Annotation
{
    public class ExecutionMethod
    {
       /// <summary>
        /// Returns a Query Expression retrieving a list of note attachments assiciated to a record
        /// </summary>
        /// <param name="entityId">The Guid of the record to return</param>
        /// <returns></returns>
        private QueryExpression GetNoteAttachmentQuery(Guid entityId)
        {  
            QueryExpression results = new QueryExpression();
            results.EntityName = "annotation";
            results.ColumnSet = new ColumnSet("subject", "filename");        

            LinkEntity linkEntity = new LinkEntity("annotation", "okn_document", "objectid", "okn_documentid", JoinOperator.Inner);
            ConditionExpression linkCondition = new ConditionExpression("okn_documentid", ConditionOperator.Equal, entityId);          
            linkEntity.LinkCriteria.AddCondition(linkCondition);           
            results.LinkEntities.Add(linkEntity);                 

            FilterExpression filter = new FilterExpression();
            filter.FilterOperator = LogicalOperator.And;
            filter.AddCondition("isdocument", ConditionOperator.Equal, new object[]{true});

            results.Criteria.AddFilter(filter);
            return results;         
           
        }  

        /// <summary>
        /// Get the Entity Reference of the Object      
        /// </summary>
        /// <param name="context">Plugin Execution Context</param>
        /// <param name="targetEntity">Target Entity</param>
        /// <returns></returns>
        private EntityReference GetEntityReferenceOfObject(IPluginExecutionContext context, Entity targetEntity)
        {
            Entity preImageEntity = null;
            EntityReference obj = null;

            if (context.MessageName == "Create")
            {
                if (targetEntity.Attributes.Contains("objectid"))
                    obj = (EntityReference)targetEntity.Attributes["objectid"];
            }

            if (context.MessageName == "Update")
            {
                if (context.PreEntityImages.Contains("pre"))
                    preImageEntity = (Entity)context.PreEntityImages["pre"];

                if (preImageEntity != null && preImageEntity.Attributes.Contains("objectid"))
                    obj = (EntityReference)preImageEntity.Attributes["objectid"];
            }

            return obj;
        }

            /// <summary>
            /// Throw an exception if a user attempts to add more than one attachment
            /// </summary>
            /// <param name="tracingService"></param>
            /// <param name="context"></param>
            /// <param name="organizationService"></param>
            /// <param name="settings"></param>
        public void DoWork(ITracingService tracingService, IPluginExecutionContext context, IOrganizationService organizationService, Settings settings)
        {

            //Guards
            if (!context.InputParameters.Contains("Target") && !(context.InputParameters["Target"] is Entity))
                return;

            // Obtain the target entity from the input parameters.
            Entity entity = (Entity)context.InputParameters["Target"];

            // Verify that the target entity represents an entity type you are expecting.                
            if (entity.LogicalName != "annotation")
                return;

            EntityReference reference = GetEntityReferenceOfObject(context, entity);

            //checkingt the regarding object of the note is document or not
            if (reference.LogicalName == "okn_document")
            {
               if(reference != null && entity.Attributes.Contains("filename")) 
                {
                  if (entity.Attributes.Contains("isdocument") && ((bool)entity.Attributes["isdocument"]))
                  {
                    var query = GetNoteAttachmentQuery(reference.Id);

                    if (query != null)
                    {
                        EntityCollection annotations = organizationService.RetrieveMultiple(query);

                        tracingService.Trace("Count : "+annotations.Entities.Count.ToString());

                        if (annotations.Entities.Count >= 1)
                            throw new InvalidPluginExecutionException("You are not allowed to add more than one document to this record.\n\n");
                    }
                  }
               }
            }
        }
    }
}
