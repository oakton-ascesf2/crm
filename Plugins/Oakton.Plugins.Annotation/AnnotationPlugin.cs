﻿using Oakton.Plugins.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;

namespace Oakton.Plugins.Annotation
{
    public class AnnotationPlugin : IPlugin
    {
        private Settings _settings;

        public AnnotationPlugin(string unsecureConfig, string secureConfig)
        {
            _settings = SettingsHelper.GetSettings(unsecureConfig, secureConfig);
        }

        public void Execute(IServiceProvider serviceProvider)
        {
            ExecutionMethods.ExecuteMethod(new ExecutionMethod().DoWork, serviceProvider, _settings);
        }
    }
}
