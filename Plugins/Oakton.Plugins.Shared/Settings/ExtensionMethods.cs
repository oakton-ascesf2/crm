﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.Shared
{
    public static class ExtensionMethods
    {
        public static string GetSetting(this Settings settings , string name)
        {
            if (settings == null)
                throw new ArgumentException("Settings");
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("name");

            var setting = settings.setting.Where(m => m.name == name).FirstOrDefault();

            if (setting != null)
                return setting.value;
            else
                return string.Empty;
        }
    }
}
