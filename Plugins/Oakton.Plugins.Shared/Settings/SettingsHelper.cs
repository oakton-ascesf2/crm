﻿namespace Oakton.Plugins.Shared
{
    public static class SettingsHelper
    {
        public static Settings GetSettings(string unsecureConfig, string secureConfig)
        {
            if (!string.IsNullOrWhiteSpace(unsecureConfig))
                return SerializationHelpers.XmlDeserializer<Settings>(unsecureConfig);
            else if (!string.IsNullOrWhiteSpace(secureConfig))
                return  SerializationHelpers.XmlDeserializer<Settings>(secureConfig);
            else
                return null;
        }
    }
}
