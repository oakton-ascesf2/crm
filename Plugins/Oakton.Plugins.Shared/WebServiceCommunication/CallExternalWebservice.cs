﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.Shared.WebServiceCommunication
{
    public static class CallExternalWebservice
    {
        public static  string CallExtService(string strUrl)
        {
            string response = string.Empty;
            using (WebClient client = new WebClient())
            {

                byte[] responseBytes = client.DownloadData(strUrl);
                response = Encoding.UTF8.GetString(responseBytes);
            }
            return response;
        }
    }
}
