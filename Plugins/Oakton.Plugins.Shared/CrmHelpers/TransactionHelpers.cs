﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.Shared.CrmHelpers
{
    public class TransactionHelpers
    {
        public void ExecuteCreateTransaction(IOrganizationService service, EntityCollection collection)
        {
            var requestToCreateRecords = new ExecuteTransactionRequest()
            {
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection(),
                ReturnResponses = true
            };

            foreach (var entity in collection.Entities)
            {
                CreateRequest createRequest = new CreateRequest { Target = entity };
                requestToCreateRecords.Requests.Add(createRequest);
            }

            var responseForCreateRecords = (ExecuteTransactionResponse)service.Execute(requestToCreateRecords);

        }

    }
}
