﻿using Microsoft.Xrm.Sdk;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.Shared
{
    public static class ServiceHelpers
    {
        /// <summary>
        /// Returns a Tracing Service
        /// </summary>
        /// <param name="serviceProvider">CRM serviceProvider</param>
        /// <returns>Returns a Tracing Service</returns>
        public static ITracingService GetTracingService(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
                throw new ArgumentException("serviceProvider");

            ITracingService result = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            return result;
        }

        /// <summary>
        /// Returns a Plugin Execution Context
        /// </summary>
        /// <param name="serviceProvider">CRM serviceProvider</param>
        /// <returns>Returns a Plugin Execution Context</returns>
        public static IPluginExecutionContext GetPluginExecutionContext(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
                throw new ArgumentException("serviceProvider");

            IPluginExecutionContext result = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            return result;
        }

        /// <summary>
        /// Returns an Organization Service
        /// </summary>
        /// <param name="serviceProvider">CRM serviceProvider</param>
        /// <returns>Returns an Organization Service</returns>
        public static IOrganizationService GetOrganizationService(IServiceProvider serviceProvider, Guid? userId = null)
        {
            if (serviceProvider == null)
                throw new ArgumentException("serviceProvider");

            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            if (serviceFactory != null)
                return serviceFactory.CreateOrganizationService(userId);

            return null;
        }

        /// <summary>
        /// Returns an Organization Service
        /// </summary>
        /// <param name="serviceProvider">CRM serviceProvider</param>
        /// <returns>Returns an Organization Service</returns>
        public static IOrganizationService GetOrganizationService(ActivityContext executionContext, Guid? userId = null)
        {
            if (executionContext == null)
                throw new ArgumentException("serviceProvider");

			IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();

            if (serviceFactory != null)
                return serviceFactory.CreateOrganizationService(userId);

            return null;
        }



    }
}
