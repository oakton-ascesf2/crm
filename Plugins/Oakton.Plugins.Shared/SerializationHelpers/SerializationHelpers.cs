﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Oakton.Plugins.Shared
{
    public static class SerializationHelpers
    {
        //Unused - Included as a comment for completeness and potential future development and use.
        public static string XmlSerializer<T>(T item)
        {
            using (StringWriter writer = new StringWriter())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(writer, item);
                return writer.ToString();
            }
        }

        /// <summary>
        /// Generic deserializer for well formed XML
        /// </summary>
        /// <typeparam name="T">Type of the object to deserialize</typeparam>
        /// <param name="response">Text string to deserialize</param>
        /// <returns>An object of type T</returns>
        public static T XmlDeserializer<T>(string response)
        {
            using (StringReader stringReader = new StringReader(response))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                return (T)xmlSerializer.Deserialize(stringReader);
            }
        }

    }
}
