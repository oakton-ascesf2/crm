﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;


namespace Oakton.Plugins.Shared.MethodExecution
{
    public class CodeActivityMethodExecution
    {
        public static void ExecuteMethod(Action<ITracingService, IWorkflowContext, IOrganizationService, CodeActivityContext, Settings> action, CodeActivityContext executionContext,Settings settings)
        {
			ITracingService tracingService = executionContext.GetExtension<ITracingService>();
			IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
            IOrganizationService service = ServiceHelpers.GetOrganizationService(executionContext, context.UserId);

            action(tracingService, context, service,executionContext, settings);

        }



    }
}
