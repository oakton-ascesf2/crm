﻿using Microsoft.Xrm.Sdk;
using System;

namespace Oakton.Plugins.Shared
{
    public class ExecutionMethods
    {
        /// <summary>
        /// Executes the Action delegate accepting the tracing service, execution context and organization service. Logs any errors. This overload
        /// </summary>
        /// <param name="action"></param>
        /// <param name="serviceProvider">Service Provider</param>
        public static void ExecuteMethod(Action<ITracingService, IPluginExecutionContext, IOrganizationService, Settings> action, IServiceProvider serviceProvider, Settings settings, bool runAsSystem = false)
        {
            ITracingService tracingService = ServiceHelpers.GetTracingService(serviceProvider);
            try
            {

                IPluginExecutionContext context = ServiceHelpers.GetPluginExecutionContext(serviceProvider);

                Guid? userId = null;
                if (!runAsSystem)
                    userId = context.UserId;

                IOrganizationService organisationService = ServiceHelpers.GetOrganizationService(serviceProvider, userId);

                action(tracingService, context, organisationService, settings);
            }
            catch (InvalidPluginExecutionException e)
            {
                tracingService.Trace(e.ToString());
                throw;
            }
            catch (Exception e)
            {
                tracingService.Trace(e.ToString());
                throw;
            }
        }

                /// <summary>
        /// Executes the Action delegate accepting the tracing service, and execution context. Logs any errors. This overload
        /// </summary>
        /// <param name="action"></param>
        /// <param name="serviceProvider">Service Provider</param>
        public static void ExecuteMethod(Action<ITracingService, IPluginExecutionContext, Settings> action, IServiceProvider serviceProvider, Settings settings, bool runAsSystem = false)
        {
            ITracingService tracingService = ServiceHelpers.GetTracingService(serviceProvider);
            try
            {

                IPluginExecutionContext context = ServiceHelpers.GetPluginExecutionContext(serviceProvider);

                Guid? userId = null;
                if (!runAsSystem)
                    userId = context.UserId;

                IOrganizationService organisationService = ServiceHelpers.GetOrganizationService(serviceProvider, userId);

                action(tracingService, context, settings);
            }
            catch (InvalidPluginExecutionException e)
            {
                tracingService.Trace(e.ToString());
                throw;
            }
            catch (Exception e)
            {
                tracingService.Trace(e.ToString());
                throw;
            }
        }
    }
}
