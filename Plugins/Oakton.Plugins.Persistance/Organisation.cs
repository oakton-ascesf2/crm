﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;

namespace Oakton.Plugins.Persistence
{
    public class Organisation
    {
        public Domain.Organisation GetOrganisation(IOrganizationService service, Guid Id)
        {
            Entity org = service.Retrieve("account", Id, new ColumnSet(true));
            return new Domain.Organisation(org);
        }
    }
}
