﻿using Microsoft.Xrm.Sdk;
using Oakton.Plugins.Shared.CrmHelpers;
using System;
using System.Collections.Generic;

namespace Oakton.Plugins.Persistence
{
    public class FinancialAssessment
    {
        public void CreateFinancialAssessmentQuestions(IOrganizationService service, List<Domain.FinancialAssessmentQuestionConfig> configQuestions, Guid financialAssessmentId, DateTime assessmentDate)
        {
            EntityCollection entities = CreateEntities(financialAssessmentId, configQuestions, assessmentDate);
            new TransactionHelpers().ExecuteCreateTransaction(service, entities);
        }

        private EntityCollection CreateEntities(Guid financialAssessmentId, List<Domain.FinancialAssessmentQuestionConfig> configQuestions, DateTime assessmentDate)
        {
            EntityCollection result = new EntityCollection();

            foreach (var q in configQuestions)
            {
                Entity e = new Entity("okn_financialassessmentquestion");

                if (q.CategoryId != null)
                 e.Attributes["okn_category"] = new EntityReference("okn_financialassessmentcategory", q.CategoryId);

                if (q.SubCategoryId != null)
                    e.Attributes["okn_subcategory"] = new EntityReference("okn_financialassessmentcategory", q.SubCategoryId);

                if (q.TopLevelCategoryId != null)
                    e.Attributes["okn_toplevelcategory"] = new EntityReference("okn_financialassessmentcategory", q.TopLevelCategoryId);
                e.Attributes["okn_assessmentdate"] = assessmentDate;
                e.Attributes["okn_criteriahigher"] = q.CriteriaHigher;
                e.Attributes["okn_criterialower"] = q.CriteriaLower;
                e.Attributes["okn_criteriamoderate"] = q.CriteriaModerate;
                e.Attributes["okn_description"] = q.Description;
                e.Attributes["okn_question"] = q.Question;
                e.Attributes["okn_questionweight"] = q.QuestionWeight;
                e.Attributes["okn_rangehighriskmax"] = q.RangeHighRiskMax;
                e.Attributes["okn_rangehighriskmin"] = q.RangeHighRiskMin;
                e.Attributes["okn_rangelowerriskmax"] =q.RangeLowerRiskMax;
                e.Attributes["okn_rangelowerriskmin"] =q.RangeLowerRiskMin;
                e.Attributes["okn_rangemoderateriskmax"] =q.RangeModerateRiskMax;
                e.Attributes["okn_rangemoderateriskmin"] =q.RangeModerateRiskMin;
                e.Attributes["okn_financialassessmentquestionsid"] = new EntityReference("okn_financialassessment", financialAssessmentId);

                e.Attributes["okn_forcata"] =q.ForCategoryA;
                e.Attributes["okn_forcatb"] =q.ForCategoryB;
                e.Attributes["okn_forcatc"] =q.ForCategoryC;

                result.Entities.Add(e);

            }
            return result;
        }
    }
}
