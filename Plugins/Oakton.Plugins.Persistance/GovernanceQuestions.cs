﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Oakton.Plugins.Domain;
using System;
using System.Collections.Generic;

namespace Oakton.Plugins.Persistence
{
    public class GovernanceQuestions
    {
        public List<GovernanceQuestion> GetQuestions(IOrganizationService service)
        {
            List<GovernanceQuestion> result = new List<GovernanceQuestion>();

            QueryExpression query = new QueryExpression("okn_governancequestion");
            query.ColumnSet = new ColumnSet(true);
            query.Distinct = true;
            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1);

            var questions = service.RetrieveMultiple(query);

            foreach (Entity entity in questions.Entities)
            {
                GovernanceQuestion question = new GovernanceQuestion(entity, service);

                if (!question.IsParent)
                {
                    GovernanceAnswerTypeEnum answerType = new GovernanceQuestions().GetParentAnswerType(service, question.ParentId);
                    question.SetParentAnswerType(answerType);
                }
                result.Add(question);
            }

            return result;
        }

        internal GovernanceAnswerTypeEnum GetParentAnswerType(IOrganizationService service, Guid parentId)
        {
            Entity e = service.Retrieve("okn_governancequestion", parentId, new ColumnSet(new string[] { "okn_answertype" }));
            OptionSetValue value = e.GetAttributeValue<OptionSetValue>("okn_answertype");
            if (value == null)
                return GovernanceAnswerTypeEnum.None;
            else
                return (GovernanceAnswerTypeEnum)(e.GetAttributeValue<OptionSetValue>("okn_answertype").Value);
        }
        public List<Guid> GetExclusions(IOrganizationService service, Guid questionId)
        {
            List<Guid> result = new List<Guid>();

            QueryExpression query = new QueryExpression("okn_governancequestion_organisation");
            query.ColumnSet = new ColumnSet(true);
            query.Distinct = true;
            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition("okn_governancequestionid", ConditionOperator.Equal, questionId);

            var exclusions = service.RetrieveMultiple(query);

            foreach (Entity entity in exclusions.Entities)
            {
                Guid id = entity.GetAttributeValue<Guid>("accountid");
                result.Add(id);
            }
            return result;
        }


    }
}
