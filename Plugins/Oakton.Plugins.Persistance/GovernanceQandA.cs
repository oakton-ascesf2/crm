﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Oakton.Plugins.Domain;
using Oakton.Plugins.Shared.CrmHelpers;
using System.Collections.Generic;

namespace Oakton.Plugins.Persistence
{
    public class GovernanceQandA
    {
        private EntityCollection CreateEntities(GovernanceAssessment assessment, List<GovernanceQuestion> questions)
        {
            EntityCollection result = new EntityCollection();

            foreach (var q in questions)
            {
                Entity e = new Entity("okn_governanceqanda");
                e.Attributes["okn_governanceassessment"] = new EntityReference(assessment.LogicalName, assessment.Id);

                e.Attributes["okn_assessmentdate"] = assessment.AssessmentDate;
                e.Attributes["okn_highanswervalue"] = q.HighAnswerValue;
                e.Attributes["okn_highestanswervalue"] = q.HighestAnswerValue;
                e.Attributes["okn_highestpercentrange"] = q.HighestPercentageRange;
                e.Attributes["okn_lowestpercentrange"] = q.LowestPercentageRange;
                e.Attributes["okn_lowanswervalue"] = q.LowAnswerValue;
                e.Attributes["okn_lowestanswervalue"] = q.LowestAnswerValue;
                e.Attributes["okn_middleanswervalue"] = q.MiddleAnswerValue;
                e.Attributes["okn_question"] = new EntityReference(q.LogicalName, q.Id);
                e.Attributes["okn_name"] = q.Name;
                e.Attributes["okn_questionvalue"] = q.QuestionValue;
                if (q.AnswerType != GovernanceAnswerTypeEnum.None)
                    e.Attributes["okn_answertype"] = new OptionSetValue((int)q.AnswerType);

                result.Entities.Add(e);
            }

            return result;

        }
        public void CreateQandARecords(IOrganizationService service, GovernanceAssessment assessment, List<GovernanceQuestion> questions)
        {
            EntityCollection entities = CreateEntities(assessment, questions);
            new TransactionHelpers().ExecuteCreateTransaction(service, entities);
        }
    }
}
