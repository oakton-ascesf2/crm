﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Oakton.Plugins.Domain;
using System.Collections.Generic;

namespace Oakton.Plugins.Persistence
{
    public class FinancialAssessmentQuestionConfig
    {
        public List<Domain.FinancialAssessmentQuestionConfig> GetApplicableQuestions(IOrganizationService service, FinancialCategoryEnum category)
        {
            List<Domain.FinancialAssessmentQuestionConfig> result = new List<Domain.FinancialAssessmentQuestionConfig>();

            QueryExpression query = new QueryExpression("okn_financeassessmentquestionconfig");
            query.ColumnSet = new ColumnSet(true);
            query.Distinct = true;
            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1);

            if (category == FinancialCategoryEnum.CategoryA)
                query.Criteria.AddCondition("okn_forcategorya", ConditionOperator.Equal, true);
            if (category == FinancialCategoryEnum.CategoryB)
                query.Criteria.AddCondition("okn_forcategoryb", ConditionOperator.Equal, true);
            if (category == FinancialCategoryEnum.CategoryC)
                query.Criteria.AddCondition("okn_forcategoryc", ConditionOperator.Equal, true);

            var questions = service.RetrieveMultiple(query);

            foreach (Entity e in questions.Entities)
            {
                result.Add(new Domain.FinancialAssessmentQuestionConfig(e));
            }

            return result;
        }

    }

}
