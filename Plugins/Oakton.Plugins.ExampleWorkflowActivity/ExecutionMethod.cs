﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Oakton.Plugins.Shared;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.ExampleWorkflowActivity
{



    public partial class ExampleCodeActivity
    {

		[Input("IntTest")]
		[Default("3")]
		public InArgument<int> IntTest { get; set; }

		[Input("OutTest")]
		[Default("3")]
		public OutArgument<int> OutTest { get; set; }

        public void DoWork(ITracingService tracingService, IWorkflowContext context, IOrganizationService organizationService, CodeActivityContext executionContext, Settings settings)
        {
            int i = IntTest.Get<int>(executionContext);
            OutTest.Set(executionContext, i + 100);
        }


    }

}

