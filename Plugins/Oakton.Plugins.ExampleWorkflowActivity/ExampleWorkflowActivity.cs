﻿using Microsoft.Xrm.Sdk.Workflow;
using Oakton.Plugins.Shared.MethodExecution;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.ExampleWorkflowActivity
{
    public partial class ExampleCodeActivity : CodeActivity
    {
        protected override void Execute(CodeActivityContext context)
        {
            CodeActivityMethodExecution.ExecuteMethod(DoWork, context, null);
        }
    }
}
