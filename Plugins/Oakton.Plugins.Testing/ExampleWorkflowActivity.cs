﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeXrmEasy;
using System.Collections.Generic;
using Oakton.Plugins.ExampleWorkflowActivity;

namespace Oakton.Plugins.Testing
{
    [TestClass]
    public class ExampleWorkflowActivity
    {
        [TestMethod]
        public void TestInOut()
        {
            var fakedContext = new XrmFakedContext();

            //Inputs
            var inputs = new Dictionary<string, object>() {{ "IntTest", 2 } };
            var result = fakedContext.ExecuteCodeActivity<ExampleCodeActivity>(inputs);
            Assert.AreEqual((int)result["OutTest"], 102);


        }
    }
}
