﻿using FakeXrmEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Oakton.Plugins.FinanceAssessment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.Testing
{
    [TestClass]
    public class FinanceAssessment
    {
        [TestMethod]
        public void TestFinanceAssessmentPlugin()
        {
            var context = new XrmRealContext()
            {
                ProxyTypesAssembly = typeof(FinanceAssessmentPlugin).Assembly,
                ConnectionStringName = "CRMServer"
            };

            Entity assessment = new Entity("okn_assessmenttype", new Guid("C2005A73-FFB5-491A-89D2-25CD577ABEE8"));

            context.Initialize(new List<Entity>() { assessment});

            var executionContext = context.GetDefaultPluginContext();

            var target = new Entity("okn_financeassessment")
            {
                Id = new Guid("C2005A73-FFB5-491A-89D2-25CD577ABEE8")
            };

            OptionSetValue option = new OptionSetValue(967300000);
            target.Attributes.Add("okn_assessmenttype", option);

            EntityReference reference = new EntityReference("account", new Guid("3E3E03AE-B05F-E711-8157-E0071B659EC1"));
            target.Attributes.Add("okn_organisation", reference);            

            executionContext.MessageName = "Create";
            executionContext.Stage = 20;
            executionContext.PrimaryEntityId = target.Id;
            executionContext.PrimaryEntityName = target.LogicalName;
            executionContext.InputParameters = new ParameterCollection()
            {
                new KeyValuePair<string, object>("Target", target)
            };

            context.ExecutePluginWith<FinanceAssessmentPlugin>(executionContext);


        }



    }
}
