﻿using FakeXrmEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Oakton.Plugins.GovernanceQandAs;
using System;
using System.Collections.Generic;

namespace Oakton.Plugins.Testing
{
    [TestClass]
    public class GovernanceQandAs
    {

        [TestMethod]
        public void TestPlugin()
        {

            var context = new XrmRealContext()
            {
                ProxyTypesAssembly = typeof(GovernanceQandAsPlugin).Assembly,
                ConnectionStringName = "CRMServer"
            };

            Entity assessment = new Entity("okn_assessmenttype", new Guid("C2005A73-FFB5-491A-89D2-25CD577ABEE8"));

            Entity a = new Entity("okn_governanceassessment")
            {
                Id = new Guid("C2005A73-FFB5-491A-89D2-25CD577ABEE8")
            };

            context.Initialize(new List<Entity>() { assessment, a});

            var executionContext = context.GetDefaultPluginContext();

            var target = new Entity("okn_governanceassessment")
            {
                Id = new Guid("C2005A73-FFB5-491A-89D2-25CD577ABEE8")
            };

            OptionSetValue option = new OptionSetValue(967300000);
            target.Attributes.Add("okn_assessmenttype", option);
            target.Attributes.Add("okn_assessmentdate", new DateTime(2016,5,21));
            EntityReference reference = new EntityReference("account", new Guid("3E3E03AE-B05F-E711-8157-E0071B659EC1"));
            target.Attributes.Add("okn_organisation", reference);           

            executionContext.MessageName = "Create";
            executionContext.Stage = 20;
            executionContext.PrimaryEntityId = target.Id;
            executionContext.PrimaryEntityName = target.LogicalName;
            executionContext.InputParameters = new ParameterCollection()
            {
                new KeyValuePair<string, object>("Target", target)
            };

            context.ExecutePluginWith<GovernanceQandAsPlugin>(executionContext);
        }

    }
}