﻿using Microsoft.Xrm.Sdk;
using Oakton.Plugins.Shared;
using System;

namespace Oakton.Plugins.Example
{
    public class ExamplePlugin : IPlugin
    {
        private Settings _settings;

        public ExamplePlugin(string unsecureConfig, string secureConfig)
        {
            _settings = SettingsHelper.GetSettings(unsecureConfig, secureConfig);
        }

        public void Execute(IServiceProvider serviceProvider)
        {
            ExecutionMethods.ExecuteMethod(new ExampleMethod().DoWork, serviceProvider, _settings);
        }
    }
}
