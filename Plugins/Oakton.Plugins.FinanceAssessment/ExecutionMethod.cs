﻿using Microsoft.Xrm.Sdk;
using Oakton.Plugins.Domain;
using Oakton.Plugins.Shared;
using System;
using System.Collections.Generic;

namespace Oakton.Plugins.FinanceAssessment
{
    public class ExecutionMethod
    {
        public void DoWork(ITracingService tracingService, IPluginExecutionContext context, IOrganizationService organizationService, Settings settings)
        {

            if (!context.InputParameters.Contains("Target") && !(context.InputParameters["Target"] is Entity))
                return;

            // Obtain the target entity from the input parameters.
            Entity entity = (Entity)context.InputParameters["Target"];

            if (entity.LogicalName != "okn_financialassessment")
                return;

            Organisation organisation = new Persistence.Organisation().GetOrganisation(organizationService, entity.GetAttributeValue<EntityReference>("okn_organisation").Id);
            List<Domain.FinancialAssessmentQuestionConfig> configQuestions = new Persistence.FinancialAssessmentQuestionConfig().GetApplicableQuestions(organizationService, organisation.FinancialCategory);

            new Persistence.FinancialAssessment().CreateFinancialAssessmentQuestions(organizationService, configQuestions, entity.Id, entity.GetAttributeValue<DateTime>("okn_assessmentdate"));
        }

    }
}
