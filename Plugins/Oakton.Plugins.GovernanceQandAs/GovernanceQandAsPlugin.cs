﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Oakton.Plugins.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.GovernanceQandAs
{
    public class GovernanceQandAsPlugin : IPlugin
    {

        public void Execute(IServiceProvider serviceProvider)
        {
            ExecutionMethods.ExecuteMethod(new ExecutionMethod().DoWork, serviceProvider, null, true);
        }

    }
}
