﻿Currently the Domain and Persistence are sitting inside this class due to the principle of YAGNI. 
If we create another plugin or two it is a good idea to move it into a Domain library.