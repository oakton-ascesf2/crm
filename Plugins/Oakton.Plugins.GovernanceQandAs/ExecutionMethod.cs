﻿using Microsoft.Xrm.Sdk;
using Oakton.Plugins.Domain;
using Oakton.Plugins.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oakton.Plugins.GovernanceQandAs
{
    public class ExecutionMethod
    {

        private List<GovernanceQuestion> GetApplicableQuestions(IOrganizationService organizationService, Organisation organisation)
        {
            List<GovernanceQuestion> result = new List<GovernanceQuestion>();

            List<GovernanceQuestion> questions = new Persistence.GovernanceQuestions().GetQuestions(organizationService);

            foreach (GovernanceQuestion q in questions)
            {
                //Question needs to be for ASPR
                if (!q.ForASPR)
                    continue;

                List<Guid> exclusions = new Persistence.GovernanceQuestions().GetExclusions(organizationService, q.Id);

                if (exclusions.Contains(organisation.Id))
                    continue;

                //If it is parent and it has children then don't add
                if (q.IsParent && questions.Where(m => m.ParentId == q.Id).Count() > 0)
                    continue;

                //All categories
                if (q.AllCategories)
                {
                    result.Add(q);
                    continue;
                }

                //Category A questions
                if (q.CategoryA && organisation.IsCategoryA)
                {
                    result.Add(q);
                    continue;
                }

                if (q.CategoryB && !organisation.IsCategoryA)
                {
                    result.Add(q);
                    continue;
                }

                if (q.Top23 && organisation.IsTop24)
                {
                    result.Add(q);
                    continue;
                }





            }

            List<GovernanceQuestion> tempList = result.OrderBy(m => m.ParentId).ToList();

            //Set Sibling Count - doing it here breaks the pattern but improves performance.
            foreach (GovernanceQuestion question in result.OrderBy(m => m.ParentId).ToList())
            {
                question.SiblingCount = tempList.Where(m => !m.IsParent && m.ParentId == question.ParentId ).ToList().Count();
                if (!question.IsParent && question.SiblingCount > 0)
                {
                    GovernanceQuestion parent = questions.Where(m => m.IsParent && m.Id == question.ParentId).FirstOrDefault();
                    question.SetValues(parent);
                }
            }


            return result;
        }

        public void DoWork(ITracingService tracingService, IPluginExecutionContext context, IOrganizationService organizationService, Settings settings)
        {
            //Guards
            if (!context.InputParameters.Contains("Target") && !(context.InputParameters["Target"] is Entity))
                return;

            // Obtain the target entity from the input parameters.
            Entity entity = (Entity)context.InputParameters["Target"];

            if (entity.LogicalName != "okn_governanceassessment")
                return;

            if (entity.GetAttributeValue<OptionSetValue>("okn_assessmenttype") == null || entity.GetAttributeValue<OptionSetValue>("okn_assessmenttype").Value != 967300000)
                return;

            GovernanceAssessment assessment = new GovernanceAssessment(entity);
            Organisation organisation = new Persistence.Organisation().GetOrganisation(organizationService, assessment.OrganisationId);
            List<GovernanceQuestion> applicableQuestions = GetApplicableQuestions(organizationService, organisation);

            new Persistence.GovernanceQandA().CreateQandARecords(organizationService, assessment, applicableQuestions);

            //Create Q&A Entities

        }

    }
}
