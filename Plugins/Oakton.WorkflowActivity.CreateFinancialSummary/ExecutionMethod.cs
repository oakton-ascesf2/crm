﻿using System;
using Oakton.Plugins.Shared;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;
using Microsoft.Xrm.Sdk.Query;


namespace Oakton.WorkflowActivity.CreateFinancialSummary
{
    public partial class CreateFinancialSummary
    {
        public void DoWork(ITracingService tracingService, IWorkflowContext context, IOrganizationService organizationService, CodeActivityContext executionContext, Settings settings)
        {   
            //Get the latest financial summary's entity collection       
            EntityCollection entColLatestFinSum= GetOrgLatestFinSum(context, tracingService, organizationService);
            //create new financial summary record using latest financial summary record's details
            Entity entNewFinSum = CreateFinancialSummaryObject(tracingService, organizationService, context, entColLatestFinSum);
            //Create new financial summary record
            organizationService.Create(entNewFinSum);
        }


        /// <summary>
        /// Gets current organization's latest financial summary
        /// </summary>
        /// <param name="context"></param>
        /// <param name="tracingService"></param>
        /// <param name="orgService"></param>
        /// <returns>Entity Collection of the latest financial summary</returns>
        private EntityCollection GetOrgLatestFinSum(IWorkflowContext context, ITracingService tracingService, IOrganizationService orgService)
        {
            EntityCollection entCollFinancialSummary = new EntityCollection();
            try
            {
                //Current organization's Id
                string strCurrentOrganization = context.PrimaryEntityId.ToString();
                
                //Query expression to get latest financial summary record of the organization
                ConditionExpression condOrganization = new ConditionExpression();
                condOrganization.AttributeName = "okn_organisation";
                condOrganization.Operator = ConditionOperator.Equal;
                condOrganization.Values.Add(strCurrentOrganization);

                FilterExpression filter1 = new FilterExpression();
                filter1.Conditions.Add(condOrganization);

                OrderExpression ordByFinancialYear = new OrderExpression();
                ordByFinancialYear.AttributeName = "okn_financialyearenddatenew";
                ordByFinancialYear.OrderType = (OrderType)1;

                QueryExpression queryFinSum = new QueryExpression("okn_financialsummary");
                queryFinSum.ColumnSet.AddColumns("okn_financialsummaryid", "okn_name", "createdon", "okn_reportingfrequency", "okn_preliminaryreporting", "okn_financialcollectiontemplaterequired", "okn_financecontact", "okn_comments", "okn_ascfunding", "okn_financialyearenddatenew", "okn_financialsummaryyear");
                queryFinSum.Criteria.AddFilter(LogicalOperator.And);
                queryFinSum.Criteria.AddFilter(filter1);
                queryFinSum.Orders.Add(ordByFinancialYear);

                entCollFinancialSummary = orgService.RetrieveMultiple(queryFinSum);
            }
            catch (Exception e)
            {
                throw new InvalidWorkflowException(e.Message);
            }
            return entCollFinancialSummary;

        }
        /// <summary>
        /// Creates financial summary's new record
        /// </summary>
        /// <param name="tracingService"></param>
        /// <param name="orgService"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private Entity CreateFinancialSummaryObject(ITracingService tracingService, IOrganizationService orgService, IWorkflowContext context, EntityCollection entCollPrevFinSummary)
        {
            Entity entFinSum = new Entity();
            try
            {               
                if (entCollPrevFinSummary.Entities.Count > 0)
                { 
                    //Assign values of the latest financial summary to the new record                  
                    entFinSum= CheckAndAssignValues(entCollPrevFinSummary, orgService, context,tracingService);                                     
                }                 
            }
            catch (Exception e)
            {
                throw new InvalidWorkflowException(e.Message);

            }
            return entFinSum;
        }

        /// <summary>
        /// Checks and assign values to new financial summary record using latest record
        /// </summary>
        /// <param name="entCollPrevFinSummary"></param>
        /// <param name="orgService"></param>
        /// <returns>Entity of new financial summary record</returns>

        private Entity CheckAndAssignValues( EntityCollection entCollPrevFinSummary, IOrganizationService orgService, IWorkflowContext context, ITracingService tracingService)
        {
            Entity entFinancialSummary = new Entity("okn_financialsummary");
            try {
                DateTime dtNewYear = ((DateTime)entCollPrevFinSummary.Entities[0].Attributes["okn_financialyearenddatenew"]).ToUniversalTime().AddYears(1);
                //Current organization's Id
                Guid currentOrganizationId = context.PrimaryEntityId;
                //Assign values from latest financial summary record to this new record
                if (entCollPrevFinSummary.Entities[0].Attributes.Contains("okn_reportingfrequency"))
                {                   
                    entFinancialSummary.Attributes["okn_reportingfrequency"] = (OptionSetValue)entCollPrevFinSummary.Entities[0].Attributes["okn_reportingfrequency"];                  
                }
                if (entCollPrevFinSummary.Entities[0].Attributes.Contains("okn_preliminaryreporting"))
                {
                    entFinancialSummary.Attributes["okn_preliminaryreporting"] = (Boolean)entCollPrevFinSummary.Entities[0].Attributes["okn_preliminaryreporting"];      
                }
                if (entCollPrevFinSummary.Entities[0].Attributes.Contains("okn_financialcollectiontemplaterequired"))
                {
                    entFinancialSummary.Attributes["okn_financialcollectiontemplaterequired"] = (Boolean)entCollPrevFinSummary.Entities[0].Attributes["okn_financialcollectiontemplaterequired"];                
                }
                if (entCollPrevFinSummary.Entities[0].Attributes.Contains("okn_financecontact"))
                {
                    entFinancialSummary.Attributes["okn_financecontact"] = new EntityReference("okn_organisationcontact", ((EntityReference)entCollPrevFinSummary.Entities[0].Attributes["okn_financecontact"]).Id);                    
                }
                if (entCollPrevFinSummary.Entities[0].Attributes.Contains("okn_comments"))
                {
                    entFinancialSummary.Attributes["okn_comments"] = entCollPrevFinSummary.Entities[0].Attributes["okn_comments"];                   
                }
                if (entCollPrevFinSummary.Entities[0].Attributes.Contains("okn_ascfunding"))
                {
                    entFinancialSummary.Attributes["okn_ascfunding"] = (OptionSetValue)entCollPrevFinSummary.Entities[0].Attributes["okn_ascfunding"];                   
                }
                if (entCollPrevFinSummary.Entities[0].Attributes.Contains("okn_financialyearenddatenew"))
                {
                    entFinancialSummary.Attributes["okn_financialyearenddatenew"] = dtNewYear;                  
                }
                string strYear = dtNewYear.Year.ToString();
                //Get the Guid of the year from the "year" entity and assign it to "year" lookup in financial summary
                Guid yearId = GetYearLookupGuid(orgService, strYear, tracingService);
                entFinancialSummary.Attributes["okn_financialsummaryyear"] = new EntityReference("okn_year", yearId);
                //Put reference to organization as current organization as the financial summary record is related to this organization
                entFinancialSummary.Attributes["okn_organisation"] = new EntityReference("account", currentOrganizationId);
            }
            catch(Exception e)
            {
                throw new InvalidWorkflowException(e.Message);

            }
            return entFinancialSummary;

        }
        /// <summary>
        /// Gets the given year's guid from year entity
        /// </summary>
        /// <param name="orgService"></param>
        /// <param name="strYear"></param>
        /// <returns></returns>
        private Guid GetYearLookupGuid(IOrganizationService orgService, string strYear,ITracingService tracingService)
        {
            Guid yearId = Guid.Empty;
            try
            {
                //Query expression to get the Guid of the "year" value from "year" entity
                ConditionExpression condYear = new ConditionExpression();
                condYear.AttributeName = "okn_name";
                condYear.Operator = ConditionOperator.Equal;
                condYear.Values.Add(strYear);

                FilterExpression filterYear = new FilterExpression();
                filterYear.Conditions.Add(condYear);

                QueryExpression qeYear = new QueryExpression("okn_year");
                qeYear.ColumnSet.AddColumns("okn_yearid", "okn_name");
                qeYear.Criteria.AddFilter(filterYear);

                EntityCollection entCollYear = orgService.RetrieveMultiple(qeYear);
                if (entCollYear.Entities.Count > 0)
                {
                    yearId = entCollYear.Entities[0].Id;
                }
            }
            catch (Exception e)
            {
                throw new InvalidWorkflowException(e.Message);
            }
            return yearId;
        }


    }
}
