﻿
using Oakton.Plugins.Shared.MethodExecution;
using System.Activities;

namespace Oakton.WorkflowActivity.CreateFinancialSummary
{
    public partial class CreateFinancialSummary : CodeActivity
    {
        protected override void Execute(CodeActivityContext context)
        {
            CodeActivityMethodExecution.ExecuteMethod(DoWork, context, null);
        }

    }
}
