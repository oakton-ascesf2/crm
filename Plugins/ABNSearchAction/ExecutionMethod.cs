﻿using System;
using Microsoft.Xrm.Sdk;
using System.Text;
using Microsoft.Xrm.Sdk.Workflow;
using Oakton.Plugins.Shared;
using System.Activities;
using System.Net;
using System.Text.RegularExpressions;
using Oakton.Plugins.Domain;
using Oakton.Plugins.Shared.WebServiceCommunication;

namespace Oakton.WorkflowActivity.ABNSearchAction
{
    public partial class ABNSearchAction
    {

        [RequiredArgument]
        [Input("ABNNumber")]
        public InArgument<string> ABNNumber { get; set; }

        [RequiredArgument]
        [Input("ACNNumber")]
        public InArgument<string> ACNNumber { get; set; }

        [RequiredArgument]
        [Input("GUID")]
        public InArgument<string> GUID { get; set; }


        [RequiredArgument]
        [Output("OrgName")]
        public OutArgument<string> OrgName { get; set; }


        [RequiredArgument]
        [Output("ExceptionMessage")]
        public OutArgument<string> ExceptionMessage { get; set; }

        public void DoWork(ITracingService tracingService, IWorkflowContext context, IOrganizationService organizationService, CodeActivityContext executionContext, Settings settings)
        {
            try
            {                
                string strOrgName = string.Empty;
                string strException = string.Empty;
                string strABNNumber = string.Empty;
                string strACNNumber = string.Empty;
                string strABNNumberWithoutSpaces = string.Empty;
                string strACNNumberWithoutSpaces = string.Empty;
                string strUrl = string.Empty;

                string strGuid = GUID.Get<string>(executionContext);               
                strABNNumber = ABNNumber.Get<string>(executionContext);
                strACNNumber = ACNNumber.Get<string>(executionContext);

                //If the workflow triggered from the change of ABN
                if ((!string.IsNullOrWhiteSpace(strABNNumber) && strABNNumber != "null")) //strABNNumber != null && strABNNumber != "" && strABNNumber != "null")
                {
                    strABNNumberWithoutSpaces = Regex.Replace(strABNNumber, @"\s+", "");                    
                     strUrl = "https://abr.business.gov.au/abrxmlsearch/abrxmlsearch.asmx/SearchByABNv201408?searchString=" + strABNNumberWithoutSpaces + "&includeHistoricalDetails=N&authenticationGuid=" + strGuid;
                }
                //If the workflow triggered from the change of ACN
                else if (strACNNumber != null && strACNNumber != "" && strACNNumber != "null")
                {                
                     strACNNumberWithoutSpaces = Regex.Replace(strACNNumber, @"\s+", "");
                    strUrl = "https://abr.business.gov.au/abrxmlsearch/AbrXmlSearch.asmx/SearchByASICv201408?searchString=" + strACNNumberWithoutSpaces + "&includeHistoricalDetails=N&authenticationGuid=" + strGuid;
                }
                if (strUrl != "")
                {
                        string strResponse = CallExternalWebservice.CallExtService(strUrl);
                        ABRPayloadSearchResults result = SerializationHelpers.XmlDeserializer<ABRPayloadSearchResults>(strResponse);
                        if (result.response.businessEntity201408 != null)
                        {
                            //success
                            if (result.response.businessEntity201408.mainName != null && HasProperty(result.response.businessEntity201408.mainName, "organisationName") == true)
                            {
                                strOrgName = result.response.businessEntity201408.mainName.organisationName;
                            }
                            else if (result.response.businessEntity201408.mainTradingName != null && HasProperty(result.response.businessEntity201408.mainTradingName, "organisationName") == true)
                            {
                                strOrgName = result.response.businessEntity201408.mainTradingName.organisationName;
                            }
                        }
                        else
                        {
                            //if abn or acn is not valid
                            if (result.response.exception != null)
                            {
                                strException = result.response.exception.exceptionDescription;
                            }

                        }
                    }
                    OrgName.Set(executionContext, strOrgName);
                    ExceptionMessage.Set(executionContext, strException);
                
            }
            catch (Exception ex)
            {

                throw new InvalidWorkflowException(ex.Message);
            }

        }

        public bool HasProperty(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName) != null;
        }
    }


}
