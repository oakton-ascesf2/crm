﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Oakton.Plugins.Shared.MethodExecution;

namespace Oakton.WorkflowActivity.ABNSearchAction
{
    public partial class ABNSearchAction : CodeActivity
    {
        protected override void Execute(CodeActivityContext context)
        {
            CodeActivityMethodExecution.ExecuteMethod(DoWork, context, null);
        }

    }
}
